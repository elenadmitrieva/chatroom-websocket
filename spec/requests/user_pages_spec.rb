require "rails_helper"
describe "User pages" do

  subject { page }

  describe "signup page" do

    before { visit signup_path }

    let(:submit) { "Create my account" }

    describe "with invalid information" do
      it "should not create a user" do
        expect { click_button submit }.not_to change(User, :count)
      end
    end

    describe "with valid information" do
      before do
        fill_in "Name",         with: "Example User"
        fill_in "Password",     with: "111111"
        fill_in "Confirmation", with: "111111"
      end

      it "should create a user" do
        expect { click_button submit }.to change(User, :count).by(1)
      end
    end
  end
  
  describe "index" do
    let(:user) { FactoryGirl.create(:user) }
    before do
      visit signin_path
      visit users_path
    end

    it { should has_title?('All users') }
    it { should has_content?('All users') }
    it "should list each user" do
      User.all.each do |user|
        expect(page).to have_selector('li', text: user.name)
      end
    end
  end
end
