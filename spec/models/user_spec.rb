require "rails_helper"
RSpec.describe User, :type => :model do
  let!(:user_test) { FactoryGirl.create(:user) }
  context "validates" do
    it "is valid with valid attributes" do
      expect(user_test).to be_valid
    end
    it "is not valid without uniqueness name" do
      user_t = User.new(:name=>"Kenny")
      expect(user_t).not_to be_valid
    end
    it "is not valid without a name" do
      user_test.name = nil
      expect(user_test).not_to be_valid
    end
    it "is not valid without a password" do
      user_test.password = nil
      expect(user_test).not_to be_valid
    end
    it "is not valid without 6 symbols a password" do
      user_test.password = '9999'
      expect(user_test).not_to be_valid
    end
    it "is not valid without confirmation a password_confirmation" do
      user_test.password_confirmation = '99999'
      expect(user_test).not_to be_valid
    end
  end
  context "on saving" do
    describe "remember token" do
      before { user_test.save }
      it {expect(user_test.remember_token).not_to be_empty}
    end
  end
end
