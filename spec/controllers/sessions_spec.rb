require "rails_helper"

RSpec.describe SessionsController, :type => :controller do
describe 'POST #create' do
  let!(:user) { FactoryGirl.create(:user) }
  context 'when password is invalid' do
    it 'renders the page with error' do
      post :create, session: { name: user.name, password: 'invalid' }
      expect(flash).not_to be_empty
      expect(response).to render_template(:new)
    end
  end
  context 'when password is valid' do
    it 'sets the user in the session and redirects them to messages' do
      post :create, session: {name: user.name, password: user.password }
      expect(response).to redirect_to '/messages'
      expect(controller.current_user).to eq user
    end
  end
end
end