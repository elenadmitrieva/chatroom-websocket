class MessagesController < ApplicationController
  before_action :signed_in_user

  def index
    @messages = Message.last(5)
  end

  def create
    @message = Message.new(message_params)
    @message.user = current_user
    @message.save
  end

  private
    def message_params
    	params.require(:message).permit(:text)
    end
 
end
 